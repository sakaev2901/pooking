import 'dart:ui';

class Constants {
  static const double padding = 15;
  static String text = 'Отель «RoomHotel» с видом на город удобно расположен в центре Сочи, рядом с Сочинским государственным цирком, Летним театром и Зимним театром. В пешей доступности популярные достопримечательности города. Расстояние до пляжа 500 м. Современный дизайн и оснащение номеров сделают ваш отдых наиболее комфортным и приятным. К услугам гостей бесплатный Wi-Fi. ';
  static List<String> urls = [
    'https://www.ggrasia.com/wp-content/uploads/2015/05/JW-Marriot-hotel-room-Galaxy-Macau-Phase-2-e1432637852679.jpg',
    'https://img2.10bestmedia.com/Images/Photos/378649/Park-Hyatt-New-York-Manhattan-Sky-Suite-Master-Bedroom-low-res_54_990x660.jpg',
    'https://media.istockphoto.com/photos/hotel-room-suite-with-view-picture-id627892060?k=20&m=627892060&s=612x612&w=0&h=k6QY-qWNlFbvYhas82e_MoSXceozjrhhgp-krujsoDw=',
    'https://q-xx.bstatic.com/xdata/images/hotel/840x460/206744855.jpg?k=8bd8700a7a922106ca04e5545d125f1f59f9d3158d372e2def0950e2a69f37dd&o=',
    'https://www.1hotels.com/sites/default/files/styles/mega_menu_image_retina/public/2021-03/1_hotel01.jpg?h=a49d782d&itok=jEfYexKd',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-T1VPQomUShQwNLVOfw5j_3wM8GlBbRd8w&usqp=CAU',
    'https://www.ggrasia.com/wp-content/uploads/2015/05/JW-Marriot-hotel-room-Galaxy-Macau-Phase-2-e1432637852679.jpg',
    'https://img2.10bestmedia.com/Images/Photos/378649/Park-Hyatt-New-York-Manhattan-Sky-Suite-Master-Bedroom-low-res_54_990x660.jpg',
    'https://media.istockphoto.com/photos/hotel-room-suite-with-view-picture-id627892060?k=20&m=627892060&s=612x612&w=0&h=k6QY-qWNlFbvYhas82e_MoSXceozjrhhgp-krujsoDw=',
    'https://q-xx.bstatic.com/xdata/images/hotel/840x460/206744855.jpg?k=8bd8700a7a922106ca04e5545d125f1f59f9d3158d372e2def0950e2a69f37dd&o=',
    'https://www.1hotels.com/sites/default/files/styles/mega_menu_image_retina/public/2021-03/1_hotel01.jpg?h=a49d782d&itok=jEfYexKd',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-T1VPQomUShQwNLVOfw5j_3wM8GlBbRd8w&usqp=CAU',
  ];

  static const Color backgroundColor = Color(0xFFECECF5);

  static const List<String> stepTitles = [
    'Введите свои данные',
    'Данные броинроваия',
    'Выберите способ оплаты',
  ];

  static const Color mainColor = Color(0xff3dc6b5);
  static const Color highlightColor = Color(0xfff9fffe);
}