import 'package:flutter/material.dart';

extension CustomTextTheme on TextTheme {
  TextStyle get heroText => const TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
    color: Colors.white,
  );


}