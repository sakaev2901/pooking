import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_form/booking_form_page.dart';
import 'package:pooking/page/hotel_room_list/widget/room_tile.dart';

import '../../base/base_button.dart';

class HotelRoomListPage extends StatelessWidget {
  const HotelRoomListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      bottomButton: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(top: BorderSide(color: Constants.backgroundColor,))
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.padding, vertical: 4),
          child: Row(
            children: [
              Expanded(child: Text('23 434 руб', style: TextStyle(fontWeight: FontWeight.bold,),),),
              Expanded(child: ButtonBase( fontSize: 18,text: 'Забронировать', onTap: () => pushNewScreen(context, screen: BookingFormPage(),),),),
            ],
          ),
        ),
      ),
      horizontalPadding: 0,
      backgroundColor: Constants.backgroundColor,
      body: Column(
        children: [
          Space.mediumH,
          RoomTile(),
          Space.mediumH,
          RoomTile(),
          Space.mediumH,
          RoomTile(),
        ],
      ),
      appBar: AppTitleBase(text: 'Выбор варианта', withBackButton: true,),
    );
  }
}
