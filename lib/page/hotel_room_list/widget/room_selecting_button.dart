import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/page/hotel_room_list/widget/dropdown_room_list.dart';

import '../../../config/constants.dart';

class RoomSelectingButton extends StatelessWidget {
  final GestureTapCallback? onClose;

  const RoomSelectingButton({
    Key? key,
    required this.onClose,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Constants.mainColor,)
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () => {},
          child: Row(
            children: [
              Expanded(
                child: DropdownRoomList(),
              ),
              InkWell(
                onTap: onClose,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(left: BorderSide(color: Constants.mainColor,))
                  ),
                  padding: const EdgeInsets.all(10),
                  child: Icon(CupertinoIcons.clear, color: Constants.mainColor, size: 18,),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
