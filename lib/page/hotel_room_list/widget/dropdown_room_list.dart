import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../config/constants.dart';

class DropdownRoomList extends StatefulWidget {

  DropdownRoomList({Key? key}) : super(key: key);

  @override
  State<DropdownRoomList> createState() => _DropdownRoomListState();
}

class _DropdownRoomListState extends State<DropdownRoomList> {
  String? selectedValue;

  List<String> items = [
    '1 номер',
    '2 номера',
    '3 номера',
    '4 номера',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DropdownButtonHideUnderline(
        child: DropdownButton2(
          customButton:  Row(
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'Выбран 1 номер',
                      style: TextStyle(
                        color: Constants.mainColor,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Icon(CupertinoIcons.chevron_down, color: Constants.mainColor, size: 18,),
              ),
            ],
          ),
          items: items
              .map((item) =>
              DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
              .toList(),
          value: selectedValue,
          onChanged: (value) {
            setState(() {
              selectedValue = value as String;
            });
          },
          buttonHeight: 40,
          buttonWidth: 140,
          itemHeight: 40,
        ),
      ),
    );
  }
}
