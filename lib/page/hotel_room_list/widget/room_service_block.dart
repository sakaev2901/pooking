import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';

class RoomServiceBlock extends StatelessWidget {
  final List<ServiceDTO> services;

  const RoomServiceBlock({Key? key, required this.services}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
        spacing: 4,
        children: _buildServices(),
      ),
    );
  }


  List<Widget> _buildServices() {
    List<Widget> widgets = [];
    services.forEach((element) {
      widgets.add(
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(element.icon),
            Space.smallW,
            Text(element.text, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12,),),
          ],
        ),
      );
    });
    return widgets;
  }
}

class ServiceDTO {
  final String text;
  final IconData icon;

  ServiceDTO({required this.text,required this.icon});


}
