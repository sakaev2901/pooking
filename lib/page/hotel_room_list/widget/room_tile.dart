import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/outlined_button_base.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/base/text_button_base.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel_room/hotel_room_page.dart';
import 'package:pooking/page/hotel_room_list/widget/room_selecting_button.dart';
import 'package:pooking/page/hotel_room_list/widget/room_service_block.dart';

class RoomTile extends StatefulWidget {
  bool selected;

  RoomTile({Key? key, this.selected = false}) : super(key: key);

  @override
  State<RoomTile> createState() => _RoomTileState();
}

class _RoomTileState extends State<RoomTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => pushNewScreen(context, screen: HotelRoomPage()),
      child: Container(
        padding: EdgeInsets.all(Constants.padding - (this.widget.selected ? 3 : 0)),
        decoration: BoxDecoration(
          color: Colors.white,
          border: this.widget.selected ? Border.all(color: Constants.mainColor, width: 3,) : Border(),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(3, 3),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Номер с 2 кроватями',
              style: TextStyle(
                fontSize: 20,
                color: Constants.mainColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            Space.smallH,
            Row(
              children: [
                Text('Цена за: '),
                Space.smallW,
                Icon(Icons.emoji_people),
                Icon(Icons.emoji_people),
                Icon(Icons.emoji_people),
              ],
            ),
            Space.smallH,
            Row(
              children: [
                Icon(CupertinoIcons.bed_double),
                Space.smallW,
                Text('2 большие кровати'),
              ],
            ),
            Space.smallH,
            Row(
              children: [
                Icon(CupertinoIcons.clock),
                Space.smallW,
                Text('Оплата на месте'),
              ],
            ),
            Space.smallH,
            Row(
              children: [
                Icon(CupertinoIcons.clock),
                Space.smallW,
                Text('Предлагается завтрак'),
              ],
            ),
            Space.mediumH,
            Divider(
              height: 0,
            ),
            Space.mediumH,
            RoomServiceBlock(services: [
              ServiceDTO(text: 'бесплатный wifi', icon: CupertinoIcons.wifi),
              ServiceDTO(text: 'ванна и душ', icon: CupertinoIcons.tornado),
              ServiceDTO(text: 'телевизор', icon: CupertinoIcons.square),
              ServiceDTO(text: 'алкобар', icon: CupertinoIcons.cart),
            ]),
            Space.mediumH,
            Divider(
              height: 0,
            ),
            Space.mediumH,
            Text(
              '21 323 руб',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
            Space.mediumH,
            TextButtonBase(
              text: 'Посмотреть подробности',
              onTap: () => pushNewScreen(context, screen: HotelRoomPage()),
              fontSize: 16,
            ),
            Space.smallH,
            if (!this.widget.selected)
              OutlinedButtonBase(text: 'Выбрать', fontSize: 18, onTap: () {
                setState(() {
                  this.widget.selected = true;
                });
              })
            else
              RoomSelectingButton(onClose: () {
                setState(() {
                  this.widget.selected = false;
                });
              },),
          ],
        ),
      ),
    );
  }
}
