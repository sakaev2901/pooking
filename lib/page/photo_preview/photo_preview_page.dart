import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import '../../base/app_title.dart';
import '../../base/base_screen.dart';

class PhotoPreviewPage extends StatelessWidget {
  final String url;

  const PhotoPreviewPage({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          AppTitleBase(text: 'Фото', withBackButton: true,),
          Expanded(
            child: Container(
                child: PhotoView(
                  backgroundDecoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  imageProvider: NetworkImage(url),
                )
            ),
          ),
        ],
      ),
    );
  }


}
