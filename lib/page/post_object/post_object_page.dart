import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:glass_kit/glass_kit.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class PostObjectPage extends StatelessWidget {
  const PostObjectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      backgroundColor: Constants.mainColor,
      body: Column(
        children: [
          Space.mediumH,
        ],
      ),
      appBar: AppTitleBase(
        text: 'Разместить объект',
        withBackButton: true,
      ),
    );
  }
}
