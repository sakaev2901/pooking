import 'package:flutter/material.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_list/widgets/booking_tile.dart';

import '../../base/app_title.dart';
import '../../base/base_screen.dart';
import '../booking_form/widgets/steps/booking_data/hotel_info.dart';

class BookingDataPage extends StatelessWidget {
  const BookingDataPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      horizontalPadding: 0,
      body: Column(
        children: [
          HotelInfo(color: Colors.transparent,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: Constants.padding,),
            child: Column(
              children: [
                ButtonBase(text: 'Отменить', onTap: () => {}, backgroundColor: Colors.red,),
              ],
            ),
          )
        ],
      ),
      appBar: AppTitleBase(text: 'Данные бронирования', withBackButton: true,),
    );
  }
}
