import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../../../base/base_button.dart';
import '../../../base/space.dart';
import '../../../config/constants.dart';
import '../../../tabs.dart';
import '../../booking_form/widgets/custom_text_input.dart';

class RegistrationStep extends StatelessWidget {
  final GestureTapCallback onRegistrationButton;
  final GestureTapCallback onVerificationButton;

  const RegistrationStep({Key? key, required this.onRegistrationButton, required this.onVerificationButton}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Space.mediumH,
      CustomTextInput(label: 'Телефон'),
      Space.mediumH,
      CustomTextInput(label: 'Пароль'),
      Space.mediumH,
      CustomTextInput(label: 'Повторите пароль'),
      Space.bigH,
      ButtonBase(
          text: 'Дальше',
          onTap: onVerificationButton),
      Space.bigH,
      Spacer(),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Уже есть аккаунт?'),
          InkWell(
            onTap: onRegistrationButton,
            child: Container(
              padding: EdgeInsets.all(4),
              child: Text(
                'Войти',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Constants.mainColor,
                ),
              ),
            ),
          ),
        ],
      )
    ]);
  }
}
