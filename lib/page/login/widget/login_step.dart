import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../../../base/base_button.dart';
import '../../../base/space.dart';
import '../../../config/constants.dart';
import '../../../tabs.dart';
import '../../booking_form/widgets/custom_text_input.dart';

class LoginStep extends StatelessWidget {
  final GestureTapCallback onRegistrationButton;

  const LoginStep({Key? key, required this.onRegistrationButton}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      CustomTextInput(label: 'Телефон'),
      Space.mediumH,
      CustomTextInput(label: 'Пароль'),
      Space.bigH,
      ButtonBase(
          text: 'Войти',
          onTap: () => pushNewScreen(
                context,
                screen: MainTabs(),
                withNavBar: true,
              )),
      Space.bigH,
      InkWell(
        onTap: () => {},
        child: Container(
          padding: EdgeInsets.all(4),
          child: Text(
            'Забыли пароль?',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: Constants.mainColor,
            ),
          ),
        ),
      ),
      Spacer(),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Впервые здесь?'),
          InkWell(
            onTap: onRegistrationButton,
            child: Container(
              padding: EdgeInsets.all(4),
              child: Text(
                'Создать аккаунт',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: Constants.mainColor,
                ),
              ),
            ),
          ),
        ],
      )
    ]);
  }
}
