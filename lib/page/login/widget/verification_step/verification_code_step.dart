import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/login/widget/verification_step/code_cell.dart';
import 'package:pooking/page/login/widget/verification_step/code_cell_list.dart';
import 'package:pooking/tabs.dart';

class VerificationCodeStep extends StatelessWidget {
  const VerificationCodeStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Space.extraH,
      Icon(CupertinoIcons.mail, color: Constants.mainColor, size: 80,),
      Space.extraH,
      Text('Мы отрпавили код подтверждения на ваше мобильный телефон +7-9**-**-**-66', textAlign: TextAlign.center, style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w700,
        color: Colors.grey,
      ),),
      Space.extraH,
      CodeCellList(),
      Space.extraH,
      CircleAvatar(
        radius: 30,
        backgroundColor: Constants.mainColor,
        child: IconButton(
          color: Constants.mainColor,
          onPressed: () => pushNewScreen(
            context,
            screen: MainTabs(),
            withNavBar: true,
          ),
          icon: Icon(
            CupertinoIcons.arrow_right,
            color: Colors.white,
          ),
        ),
      ),
      Space.extraH,
      Text('Не получили код?', style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w600,),),
      Space.mediumH,
      InkWell(onTap: () => {},child: Text('Отправить еще раз', style: TextStyle(fontSize: 15, color: Constants.mainColor, fontWeight: FontWeight.w700,),)),
    ]);
  }
}
