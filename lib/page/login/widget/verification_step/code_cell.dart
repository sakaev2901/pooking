import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pooking/config/constants.dart';

class CodeCell extends StatelessWidget {
  ValueChanged<String>? onChanged;
  final FocusNode focusNode;

  CodeCell({Key? key, this.onChanged, required this.focusNode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: MediaQuery.of(context).size.width / 7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        border: Border.all(color: Constants.mainColor, width: 2,)
      ),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              focusNode: focusNode,
              inputFormatters: [
                LengthLimitingTextInputFormatter(1),
              ],
              onChanged: onChanged,
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.black54,

              ),
              decoration: InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.all(0),
                border: InputBorder.none,
                // hintText: hint,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
