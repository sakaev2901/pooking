import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/login/widget/verification_step/code_cell.dart';
import 'package:pooking/tabs.dart';

class CodeCellList extends StatefulWidget {

  CodeCellList({Key? key}) : super(key: key);

  @override
  State<CodeCellList> createState() => _CodeCellListState();
}

class _CodeCellListState extends State<CodeCellList> {
  int cellIndex = 0;
  final FocusNode firstNode = FocusNode();
  final FocusNode secondNode = FocusNode();
  final FocusNode thirdNode = FocusNode();
  final FocusNode fourthNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        CodeCell(focusNode: firstNode, onChanged: (text) {
            if (text.length == 1) {
              FocusScope.of(context).requestFocus(secondNode);

          } else {
              FocusManager.instance.primaryFocus?.unfocus();
            }
        },),
        Space.mediumW,
        CodeCell(focusNode: secondNode,onChanged: (text) {
          if (text.length == 1) {
            FocusScope.of(context).requestFocus(thirdNode);

          } else {
            FocusScope.of(context).requestFocus(firstNode);

          }
        },),
        Space.mediumW,
        CodeCell(focusNode: thirdNode,onChanged: (text) {
          if (text.length == 1) {
            FocusScope.of(context).requestFocus(fourthNode);
          } else {
            FocusScope.of(context).requestFocus(secondNode);

          }
        },),
        Space.mediumW,
        CodeCell(focusNode: fourthNode,onChanged: (text) {
          if (text.length == 1) {
            FocusManager.instance.primaryFocus?.unfocus();
            pushNewScreen(context, screen: MainTabs(), withNavBar: true,);
          } else {
            FocusScope.of(context).requestFocus(thirdNode);

          }
        },),
      ],
    );
  }
}
