import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/base/text_button_base.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_form/widgets/custom_text_input.dart';
import 'package:pooking/page/home/home_page.dart';
import 'package:pooking/page/login/widget/login_step.dart';
import 'package:pooking/page/login/widget/registration_step.dart';
import 'package:pooking/page/login/widget/verification_step/verification_code_step.dart';
import 'package:pooking/tabs.dart';

import '../../base/base_screen.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool showLoginStep = true;
  double _fontSize = 80;
  late Widget _child;

  @override
  void initState() {
    _child = LoginStep(
      onRegistrationButton: onRegistrationStep,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(Constants.padding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AnimatedContainer(
                curve: Curves.decelerate,
                height: showLoginStep ? 230 : 60,
                width: double.infinity,
                duration: Duration(milliseconds: 400),
                child: AnimatedAlign(
                  alignment: showLoginStep
                      ? Alignment.bottomCenter
                      : Alignment.topCenter,
                  duration: Duration(milliseconds: 200),
                  child: AnimatedDefaultTextStyle(
                      curve: Curves.easeInToLinear,
                      child: Text(
                        'BaR',
                      ),
                      duration: Duration(milliseconds: 200),
                      style: TextStyle(
                        color: Constants.mainColor,
                        fontWeight: FontWeight.bold,
                        fontSize: _fontSize,
                      )),
                ),
              ),
              Space.bigH,
              Expanded(
                child: AnimatedSwitcher(
                  duration: Duration(milliseconds: 200),
                  child: _child,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onRegistrationStep() {
    setState(() {
      _child = RegistrationStep(
        onRegistrationButton: onLoginStep,
        onVerificationButton: onVerificationStep,
      );
      showLoginStep = false;
      _fontSize = 35;
    });
  }

  void onLoginStep() {
    setState(() {
      _child = LoginStep(
        onRegistrationButton: onRegistrationStep,
      );
      showLoginStep = true;
      _fontSize = 80;
    });
  }

  void onVerificationStep() {
    setState(() {
      _child = VerificationCodeStep();
      // showLoginStep = false;
      // _fontSize = 35;
    });
  }
}
