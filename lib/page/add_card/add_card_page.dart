import 'package:awesome_card/credit_card.dart';
import 'package:awesome_card/extra/card_type.dart';
import 'package:awesome_card/style/card_background.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/add_card/card_inputs.dart';
import 'package:pooking/page/booking_form/widgets/custom_text_input.dart';

import '../../base/app_title.dart';
import '../../base/base_screen.dart';

class AddCardPage extends StatelessWidget {
  final TextEditingController cardController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController dateController = TextEditingController();
  final TextEditingController cvvController = TextEditingController();

  AddCardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      horizontalPadding: 0,
      body: Column(
        children: [
          Space.mediumH,
          CreditCard(
              cardNumber: cardController.text,
              cardExpiry: "10/25",
              cardHolderName: "Card Holder",
              cvv: "456",
              bankName: "Axis Bank",
              cardType: CardType.other, // Optional if you want to override Card Type
              showBackSide: false,
              frontBackground: CardBackgrounds.black,
              backBackground: CardBackgrounds.white,
              showShadow: true,
              textExpDate: 'Exp. Date',
              textName: 'Name',
              textExpiry: 'MM/YY'
          ),
          Space.mediumH,
          CardInputs(
            cardController: cardController,
            cvvController: cvvController,
            nameController: nameController,
            dateController: dateController,
          ),
          Space.mediumH,
          Padding(padding: EdgeInsets.symmetric(horizontal: Constants.padding,),child: ButtonBase(text: 'Добавить ', onTap: () => Navigator.of(context).pop())),
        ],
      ),
      appBar: AppTitleBase(text: 'Добавить карту', withBackButton: true,),
    );
  }
}
