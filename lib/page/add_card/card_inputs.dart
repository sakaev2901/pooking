import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

import '../booking_form/widgets/custom_text_input.dart';

class CardInputs extends StatelessWidget {
  final TextEditingController cardController;
  final TextEditingController nameController;
  final TextEditingController dateController;
  final TextEditingController cvvController;

  const CardInputs({Key? key, required this.nameController, required this.cardController, required this.cvvController, required this.dateController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: Constants.padding,),
      child: Column(
        children: [
          CustomTextInput(label: 'Номер карты', controller: cardController,),
          Space.mediumH,
          CustomTextInput(label: 'Держатель', controller: nameController,),
          Space.mediumH,
          Row(
            children: [
              Expanded(child: CustomTextInput(label: 'Дата', controller: dateController,)),
              Space.mediumW,
              Expanded(child: CustomTextInput(label: 'CVV', controller: cvvController,)),
          ],),
        ],
      ),
    );
  }
}
