import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class DescriptionBlock extends StatelessWidget {

  const DescriptionBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Constants.padding,),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Описание', style: TextStyle(fontWeight: FontWeight.bold,),),
          SizedBox(height: 8,),
          Text(Constants.text),
        ],
      ),
    );
  }
}
