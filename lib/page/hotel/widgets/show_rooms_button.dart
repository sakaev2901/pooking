import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../../../base/base_button.dart';
import '../../../config/constants.dart';
import '../../hotel_room_list/hotel_room_list_page.dart';

class ShowRoomsButton extends StatelessWidget {
  const ShowRoomsButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: Constants.padding, vertical: 4),
        child: IntrinsicHeight(
          child: Row(
            children: [
              Container(
                height: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                  color: Constants.mainColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Icon(
                  CupertinoIcons.heart_fill,
                  color: Colors.white,
                ),
              ),
              SizedBox(width: 16,),
              Expanded(
                child: ButtonBase(
                  text: 'Посмотреть номера',
                  onTap: () => pushNewScreen(context, screen: HotelRoomListPage()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
