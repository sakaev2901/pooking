import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class ServicesBlock extends StatelessWidget {
  const ServicesBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(Constants.padding),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Удобства', style: TextStyle(fontWeight: FontWeight.bold,),),
          SizedBox(height: 8,),
          Wrap(
            spacing: 4,
            runSpacing: 4,
            children: [
              ServiceTile(text: 'Парковка'),
              ServiceTile(text: 'Номера для некурящих'),
              ServiceTile(text: 'Семейные номера'),
              ServiceTile(text: 'Бесплатный WiFi'),
            ],
          )
        ],
      ),
    );
  }
}

class ServiceTile extends StatelessWidget {
  final String text;

  const ServiceTile({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(CupertinoIcons.info),
          SizedBox(width: 4,),
          Text(text),
        ],
      ),
    );
  }
}
