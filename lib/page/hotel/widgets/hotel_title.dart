import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class HotelTitle extends StatelessWidget {
  final String name;
  final double reviewGrade;

  const HotelTitle({Key? key, required this.name, required this.reviewGrade})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(Constants.padding),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Text(name,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
              StarsBlock(),
            ],
          ),
          Spacer(),
          ReviewGrade(grade: reviewGrade),
        ],
      ),
    );
  }
}

class ReviewGrade extends StatelessWidget {
  final double grade;

  const ReviewGrade({Key? key, required this.grade}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Constants.mainColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Center(
        child: Text(grade.toString(),
          style: TextStyle(fontSize: 18, color: Colors.white),),
      ),
    );
  }
}

class StarsBlock extends StatelessWidget {
  const StarsBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: _buildStars(4),
      ),
    );
  }

  List<Widget> _buildStars(int count) {
    List<Widget> widgets = [];
    for (int i = 0; i < count; i++) {
      widgets.add(Icon(Icons.star_rounded, color: Colors.yellow, size: 19,));
    }
    return widgets;
  }
}
