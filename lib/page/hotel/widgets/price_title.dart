import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class PriceTitle extends StatelessWidget {
  const PriceTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: EdgeInsets.all(Constants.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Цена за 8 ночей и 3 взрослых', style: TextStyle(),),
          Text('23 454 руб', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
        ],
      ),
    );
  }
}
