import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel/widgets/hotel_title.dart';
import 'package:pooking/page/review_list/review_list_page.dart';

class ReviewBlock extends StatelessWidget {
  const ReviewBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => pushNewScreen(context, screen: ReviewListPage()),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(Constants.padding),
        child: Row(
          children: [
            ReviewGrade(grade: 8.8),
            SizedBox(width: 8,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Хорошо', style: TextStyle(fontWeight: FontWeight.bold,),),
                Text('Посмотреть 33 отзывов'),
              ],
            ),
            Spacer(),
            Icon(CupertinoIcons.forward,)
          ],
        ),
      ),
    );
  }
}
