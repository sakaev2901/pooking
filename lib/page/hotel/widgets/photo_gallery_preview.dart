import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/photo_gallery/photo_gallery_page.dart';

class PhotoGalleryPreview extends StatelessWidget {
  final List<String> urls;

  const PhotoGalleryPreview({Key? key, required this.urls}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => pushNewScreen(context, screen: PhotoGalleryPage(urls: Constants.urls,), ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: Column(
          children: [
        Container(
          height: MediaQuery.of(context).size.width * 0.3,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: double.infinity,
                  child: PhotoWithBorderRadius(url: urls[0],),
                ),
              ),
              SizedBox(
                width: 4,
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                  child: PhotoWithBorderRadius(url: urls[1],),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Container(
          height: MediaQuery.of(context).size.width * 0.3,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: double.infinity,
                  child: PhotoWithBorderRadius(url: urls[2],),
                ),
              ),
              SizedBox(
                width: 4,
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                  child: PhotoWithBorderRadius(url: urls[3],),
                ),
              ),
              SizedBox(
                width: 4,
              ),
              PhotoWithCount(
                url: urls[4],
              ),
            ],
          ),
        )
          ],
        ),
      ),
    );
  }
}

class PhotoWithCount extends StatelessWidget {
  final String url;

  const PhotoWithCount({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            child: PhotoWithBorderRadius(url: url,),
          ),
          Container(
            decoration: BoxDecoration(
                color: Color(0x90444343),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(
              child: Text('+10', style: TextStyle(fontSize: 28, color: Colors.white, fontWeight: FontWeight.bold,),),
            ),
          )
        ],
      ),
    );
  }
}

class PhotoWithBorderRadius extends StatelessWidget {
  final String url;

  const PhotoWithBorderRadius({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Image.network(
        url,
        fit: BoxFit.cover,
      ),
    );
  }
}

