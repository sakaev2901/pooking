import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel/widgets/description_block.dart';
import 'package:pooking/page/hotel/widgets/hotel_title.dart';
import 'package:pooking/page/hotel/widgets/photo_gallery_preview.dart';
import 'package:pooking/page/hotel/widgets/price_title.dart';
import 'package:pooking/page/hotel/widgets/review_block.dart';
import 'package:pooking/page/hotel/widgets/services_block.dart';
import 'package:pooking/page/hotel/widgets/show_rooms_button.dart';

import '../hotel_room_list/hotel_room_list_page.dart';

class HotelPage extends StatelessWidget {
  const HotelPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      backgroundColor: Color(0xFFECECF5),
      bottomButton: ShowRoomsButton(),
      horizontalPadding: 0,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 4,),
          PhotoGalleryPreview(urls: Constants.urls,),
          SizedBox(height: 4,),
          HotelTitle(name: "Hotel Name", reviewGrade: 8.8,),
          SizedBox(height: 4,),
          PriceTitle(),
          SizedBox(height: 4,),
          ReviewBlock(),
          SizedBox(height: 4,),
          DescriptionBlock(),
          SizedBox(height: 4,),
          ServicesBlock(),
          SizedBox(height: 4,),
        ],
      ),
      appBar: AppTitleBase(text: 'Hotel Name', withBackButton: true,),
    );
  }
}
