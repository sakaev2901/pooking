import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/page/photo_preview/photo_preview_page.dart';

import '../../base/app_title.dart';
import '../../base/base_button.dart';
import '../../base/base_screen.dart';
import '../../config/constants.dart';
import '../hotel_room_list/hotel_room_list_page.dart';

class PhotoGalleryPage extends StatelessWidget {
  final List<String> urls;

  const PhotoGalleryPage({Key? key, required this.urls}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      horizontalPadding: 4,
      bottomButton: Container(
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.padding, vertical: 4),
          child: ButtonBase(text: 'Посмотреть номера', onTap: () => pushNewScreen(context, screen: HotelRoomListPage()),),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: Wrap(
          runSpacing: 4,
          children: _buildPhotos(context),
        ),
      ),
      appBar: AppTitleBase(text: 'Фотографии', withBackButton: true,),
    );
  }

  List<Widget> _buildPhotos(BuildContext context) {
    List<Widget> widgets = [];
    urls.forEach((element) {
      widgets.add(InkWell(
        onTap: () => pushNewScreen(context, screen: PhotoPreviewPage(url: element), pageTransitionAnimation: PageTransitionAnimation.fade),
        child: Container(
          width: double.infinity,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.network(
              element,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),);
    });
    return widgets;
  }
}
