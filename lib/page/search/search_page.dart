import 'package:flutter/material.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/search/widgets/location_list.dart';
import 'package:pooking/page/search/widgets/search_input.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      backgroundColor: Constants.highlightColor,
      body: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).padding.top,),
          SizedBox(height: 20,),
          SearchInput(),
          SizedBox(height: 20,),
          LocationList(),
        ],
      ),
    );
  }
}
