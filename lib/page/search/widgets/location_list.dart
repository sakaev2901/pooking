import 'package:flutter/material.dart';

import '../../../config/constants.dart';

class LocationList extends StatelessWidget {
  const LocationList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        LocationTile(),
        SizedBox(height: 10,),
        LocationTile(),
        SizedBox(height: 10,),
        LocationTile(),
      ],
    );
  }

}

class LocationTile extends StatelessWidget {
  const LocationTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
            color: Color(0x6F9CFFFF)
          ),
          child: Icon(
            Icons.location_city,
            color: Constants.mainColor,
          ),
        ),
        SizedBox(width: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Москва', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
            Text('Дополнительная информация', style: TextStyle(),),
          ],
        )
      ],
    );
  }
}

