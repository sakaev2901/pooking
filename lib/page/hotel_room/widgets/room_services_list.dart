import 'package:flutter/material.dart';
import 'package:pooking/page/hotel_room/widgets/room_service.dart';

import '../../../base/space.dart';

class RoomServicesList extends StatelessWidget {
  const RoomServicesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Услуги и удобства', style: TextStyle(fontSize: 16,),),
          Space.mediumH,
          RoomService(icon: Icons.kitchen, services: ['Кофемашина', "Холодильник"], title: 'Кухня'),
          RoomService(icon: Icons.bathroom_outlined, services: ['Бесплатные принадлежности', "Ванна и душ", 'Ванная комната', 'Туалет', 'Фен'], title: 'Ванная комната'),
          RoomService(icon: Icons.info_outline, services: ['Курение запрещено', "Гладильные принадлежности", 'Кондиционер', 'Сейф', 'Утюг'], title: 'Общие'),
          RoomService(icon: Icons.tv, services: ['Кабельные каналы', "Телефон"], title: 'Медиа и технологии'),
        ],
      ),
    );
  }
}
