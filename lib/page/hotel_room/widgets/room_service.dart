import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';

class RoomService extends StatelessWidget {
  final String title;
  final IconData icon;
  final List<String> services;

  const RoomService({Key? key, required this.icon, required this.services, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(icon),
              Space.mediumW,
              Text(title, style: TextStyle(fontSize: 16, fontWeight:  FontWeight.bold,),),
            ],
          ),
          Space.mediumH,
          ..._buildServices(),
          Space.mediumH,
        ],
      ),
    );
  }

  List<Widget> _buildServices() {
    return services.map((e) => Text(e)).toList();
  }
}
