import 'package:flutter/material.dart';

class RoomPhotoGallery extends StatelessWidget {
  const RoomPhotoGallery({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 4,
      child: PageView(
        children: [
          Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-T1VPQomUShQwNLVOfw5j_3wM8GlBbRd8w&usqp=CAU', fit: BoxFit.cover,),
          Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-T1VPQomUShQwNLVOfw5j_3wM8GlBbRd8w&usqp=CAU', fit: BoxFit.cover,),
          Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-T1VPQomUShQwNLVOfw5j_3wM8GlBbRd8w&usqp=CAU', fit: BoxFit.cover,),
        ],
      ),
    );
  }
}
