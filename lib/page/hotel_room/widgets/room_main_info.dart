import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel_room/widgets/room_photo_gallery.dart';

import '../../../base/outlined_button_base.dart';
import '../../../base/space.dart';
import '../../hotel_room_list/widget/room_selecting_button.dart';

class RoomMainInfo extends StatefulWidget {
  bool selected;

  RoomMainInfo({Key? key, this.selected = false}) : super(key: key);

  @override
  State<RoomMainInfo> createState() => _RoomMainInfoState();
}

class _RoomMainInfoState extends State<RoomMainInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RoomPhotoGallery(),
          Space.mediumH,
         Padding(
           padding: EdgeInsets.symmetric(horizontal: Constants.padding),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text('Номер с двуся кроватями размера queen size', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,),),
               Space.mediumH,
               Row(
                 children: [
                   Text('Цена за: '),
                   Space.smallW,
                   Icon(Icons.emoji_people),
                   Icon(Icons.emoji_people),
                   Icon(Icons.emoji_people),
                 ],
               ),
               Space.mediumH,
               Divider(height: 0,),
               Space.mediumH,
               Text('Цена за 5 ночей'),
               Text('23 232 руб', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
               Space.mediumH,
               Divider(height: 0,),
               Space.mediumH,
               if (!this.widget.selected)
                 OutlinedButtonBase(text: 'Выбрать', fontSize: 18, onTap: () {
                   setState(() {
                     this.widget.selected = true;
                   });
                 })
               else
                 RoomSelectingButton(onClose: () {
                   setState(() {
                     this.widget.selected = false;
                   });
                 },),
             ],
           ),
         ),
        ],
      ),
    );
  }
}
