import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel_room/widgets/room_services_list.dart';

import '../../../base/space.dart';

class RoomExtraInfo extends StatelessWidget {
  const RoomExtraInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Constants.padding),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Описание номера', style: TextStyle(fontWeight: FontWeight.bold,),),
          Space.mediumH,
          Text('Этот номер оснащен телевизором с плоским экраном и кабельными каналами, принадлежности для кофе и небольшим холодильником'),
          Space.mediumH,
          RoomServicesList(),
        ],
      ),
    );
  }
}
