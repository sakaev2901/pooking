import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel_room/widgets/room_extra_info.dart';
import 'package:pooking/page/hotel_room/widgets/room_main_info.dart';
import 'package:pooking/page/hotel_room/widgets/room_photo_gallery.dart';

import '../../base/base_button.dart';
import '../../base/space.dart';
import '../booking_form/booking_form_page.dart';

class HotelRoomPage extends StatelessWidget {
  const HotelRoomPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      bottomButton: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(top: BorderSide(color: Constants.backgroundColor,))
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.padding, vertical: 4),
          child: Row(
            children: [
              Expanded(child: Text('23 434 руб', style: TextStyle(fontWeight: FontWeight.bold,),),),
              Expanded(child: ButtonBase( fontSize: 18,text: 'Забронировать', onTap: () => pushNewScreen(context, screen: BookingFormPage(),),),),
            ],
          ),
        ),
      ),
      backgroundColor: Constants.backgroundColor,
      horizontalPadding: 0,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RoomMainInfo(),
          Space.mediumH,
          RoomExtraInfo(),
        ],
      ),
      appBar: AppTitleBase(text: 'Информация о номере', withBackButton: true,),
    );
  }
}
