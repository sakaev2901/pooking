import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/card_base.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/hotel/hotel_page.dart';
import 'package:pooking/page/hotel/widgets/hotel_title.dart';
import 'package:pooking/page/hotel_list/favorite_icon_button.dart';
import 'package:pooking/page/hotel_list/hotel_name_block.dart';
import 'package:pooking/page/hotel_list/hotel_review_block.dart';
import 'package:pooking/page/hotel_list/location_block.dart';
import 'package:pooking/page/hotel_list/price_block.dart';

import '../../config/constants.dart';

class HotelItem extends StatelessWidget {
  final bool liked;

  const HotelItem({Key? key, this.liked = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardBase(
      onTap: () => pushNewScreen(context, screen: HotelPage(), withNavBar: false,),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                constraints: BoxConstraints(
                  minHeight: double.infinity,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network(
                    'https://cf.bstatic.com/xdata/images/hotel/max1024x768/184305239.jpg?k=2d22fe63ae1f8960e057238c98fb436f7bd9f65854e3a5e918607c5cfa1d0a52&o=&hp=1',
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(padding:EdgeInsets.only(top: 8) ,child: HotelNameBlock()),
                            StarsBlock(),
                          ],
                        ),
                        Spacer(),
                        FavoriteIconButton(liked: liked,),
                      ],
                    ),
                    Space.bigH,
                    HotelPriceBlock(),
                    Space.bigH,
                    LocationBlock(),
                    Space.mediumH,
                    HotelReviewBlock(),
                    Space.mediumH,
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
