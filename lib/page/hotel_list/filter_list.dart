import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/feature/sorting_sheet/sorting_sheet.dart';
import 'package:pooking/page/filter/filter_page.dart';

class FilterList extends StatelessWidget {
  const FilterList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(

      children: [
        FilterTile(
          text: 'Сортировка',
          iconData: CupertinoIcons.arrow_up_arrow_down,
          onTap: () => showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (_) => SortingSheet(),
          ),
        ),
        FilterTile(
            text: 'Фильтр',
            iconData: CupertinoIcons.slider_horizontal_3,
            onTap: () => pushNewScreen(context, screen: FilterPage())),
        FilterTile(
            text: 'На карте', iconData: CupertinoIcons.map, onTap: () => {}),
      ],
    );
  }
}

class FilterTile extends StatelessWidget {
  final IconData iconData;
  final GestureTapCallback onTap;
  final String text;

  const FilterTile({
    Key? key,
    required this.text,
    required this.iconData,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(iconData, color: Colors.grey,),
              SizedBox(
                width: 5,
              ),
              Text(text, style: TextStyle(fontWeight: FontWeight.bold,),),
            ],
          ),
        ),
      ),
    );
  }
}
