import 'package:flutter/material.dart';

import '../../config/constants.dart';

class LocationBlock extends StatelessWidget {
  const LocationBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   Row(
      children: [
        Icon(
          Icons.location_on_outlined,
          size: 20,
          color: Constants.mainColor,
        ),
        Text(
          '13 км от центра',
          style: TextStyle(
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}
