import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class FavoriteIconButton extends StatefulWidget {
  bool liked;

  FavoriteIconButton({Key? key, this.liked = false}) : super(key: key);

  @override
  _FavoriteIconButtonState createState() => _FavoriteIconButtonState();
}

class _FavoriteIconButtonState extends State<FavoriteIconButton> {
  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Material(
        child: IconButton(
          padding: EdgeInsets.all(8),
          constraints: BoxConstraints(),
          onPressed: () => setState(() {
            this.widget.liked = !this.widget.liked;
          }),
          icon: this.widget.liked
              ? Icon(
                  CupertinoIcons.heart_fill,
                  color: Constants.mainColor,
                )
              : Icon(
                  CupertinoIcons.heart,
                  color: Colors.grey,
                ),
        ),
      ),
    );
  }
}
