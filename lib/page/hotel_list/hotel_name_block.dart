import 'package:flutter/material.dart';

class HotelNameBlock extends StatelessWidget {
  const HotelNameBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return     Text(
      'Hotel name',
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
