import 'package:flutter/material.dart';

import '../../config/constants.dart';

class HotelReviewBlock extends StatelessWidget {
  const HotelReviewBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.all(3),
          decoration: BoxDecoration(
            color: Constants.mainColor,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Center(
            child: Text(
              '8.8',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          '234 отзывов',
          style: TextStyle(
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}
