import 'package:flutter/material.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/page/hotel_list/filter_list.dart';
import 'package:pooking/page/hotel_list/hotel_item.dart';
import 'package:pooking/page/search/widgets/search_input.dart';

class HotelListPage extends StatelessWidget {
  const HotelListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(body: Column(
      children: [
        SizedBox(height: MediaQuery.of(context).padding.top,),
        SizedBox(height: 20,),
        SearchInput(),
        SizedBox(height: 10,),
        FilterList(),
        SizedBox(height: 10,),
        HotelItem(),
        SizedBox(height: 10,),
        HotelItem(),
        SizedBox(height: 10,),
        HotelItem(),
        SizedBox(height: 10,),
        HotelItem(),
        SizedBox(height: 10,),
        HotelItem(),
        SizedBox(height: 10,),
        HotelItem(),

      ],
    ), showSafeAreaTopPadding: true,);
  }
}
