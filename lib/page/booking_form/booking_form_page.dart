import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/booking_form/widgets/alertion_text.dart';
import 'package:pooking/page/booking_form/widgets/booking_form.dart';
import 'package:pooking/page/booking_form/widgets/custom_text_input.dart';
import 'package:pooking/page/booking_form/widgets/step_title.dart';
import 'package:pooking/page/booking_form/widgets/steps/booking_data/booking_data_step.dart';
import 'package:pooking/page/booking_form/widgets/steps/form_step.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/payment_step.dart';

import '../../base/app_title.dart';
import '../../base/base_button.dart';
import '../../base/base_screen.dart';
import '../../config/constants.dart';

class BookingFormPage extends StatefulWidget {
  const BookingFormPage({Key? key}) : super(key: key);

  @override
  State<BookingFormPage> createState() => _BookingFormPageState();
}

class _BookingFormPageState extends State<BookingFormPage> {

  int stepIndex = 0;
  List<Widget> stepPages = [
    FormStep(),
    BookingDataStep(),
    PaymentStep(),
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (stepIndex > 0) {
          setState(() {
            stepIndex--;
          });
          return false;
        } else {
          return true;
        }
      },
      child: BaseScreen(
        backgroundColor: Constants.backgroundColor,
        horizontalPadding: 0,
        bottomButton: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(top: BorderSide(color: Constants.backgroundColor,))
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Constants.padding, vertical: 4),
            child: Row(
              children: [
                Expanded(child: Text('23 434 руб', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),),
                Expanded(child: ButtonBase( fontSize: 18,text: stepIndex < stepPages.length - 1 ? 'Следующий шаг' : 'Забронировать', onTap: _moveStep),),
              ],
            ),
          ),
        ),
        body: stepPages[stepIndex],
        appBar: StepTitle(stepIndex: stepIndex, onBackButton: _onBackButton, stepTitles: Constants.stepTitles,),
      ),
    );
  }

  void _moveStep() {
    setState(() {
      if (stepIndex < stepPages.length - 1)
      stepIndex++;
    });
  }

  void _onBackButton() {
    if (stepIndex > 0) {
      setState(() {
        stepIndex--;
      });
    } else {
      Navigator.of(context).pop();
    }
  }

}
