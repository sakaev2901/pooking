import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

import '../../../base/space.dart';
import 'custom_text_input.dart';

class BookingForm extends StatelessWidget {
  const BookingForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Constants.padding),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextInput(
            label: 'Имя',
          ),
          Space.bigH,
          CustomTextInput(
            label: 'Фамилия',
          ),
          Space.bigH,
          CustomTextInput(
            label: 'Электронный адрес',
          ),
          Space.bigH,
          CustomTextInput(
            label: 'Мобильный телефон',
          ),
        ],
      ),
    );
  }
}
