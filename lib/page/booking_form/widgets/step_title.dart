import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_form/widgets/step_progress.dart';

class StepTitle extends StatelessWidget {
  final int stepIndex;
  final GestureTapCallback onBackButton;
  final List<String> stepTitles;

  const StepTitle({Key? key, required this.stepIndex, required this.onBackButton, required this.stepTitles}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).padding.top + 15,
        left: 15,
        right: 15,
        bottom: 15,
      ),
      decoration: BoxDecoration(color: Constants.mainColor),
      child: Row(
        children: [
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: onBackButton,
            child: Padding(
              padding: const EdgeInsets.only(right: 15),
              child: const Icon(CupertinoIcons.back, color: Colors.white,),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                stepTitles[stepIndex],
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Space.mediumH,
              StepProgress(stepIndex: stepIndex, stepsCount: 3,),
            ],
          ),
        ],
      ),
    );
  }
}
