import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StarsGrade extends StatelessWidget {
  const StarsGrade({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  Row(
        children: [
          Icon(CupertinoIcons.star_fill, color: Colors.yellow, size: 18,),
          Icon(CupertinoIcons.star_fill, color: Colors.yellow, size: 18,),
          Icon(CupertinoIcons.star_fill, color: Colors.yellow, size: 18,),
        ],
      ),
    );
  }
}
