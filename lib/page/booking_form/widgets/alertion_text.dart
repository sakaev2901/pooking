import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class AlertingText extends StatelessWidget {
  const AlertingText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: EdgeInsets.all(Constants.padding),
      child: Row(
        children: [
          Icon(CupertinoIcons.exclamationmark_circle, color: Colors.orange,),
          Space.mediumW,
          Text('Заполните все необходимые поля'),
        ],
      ),
    );
  }
}
