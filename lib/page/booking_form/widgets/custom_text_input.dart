import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class CustomTextInput extends StatefulWidget {
  final String label;
  TextEditingController? controller;

  CustomTextInput({Key? key, required this.label, this.controller}) : super(key: key);

  @override
  State<CustomTextInput> createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput> {
  final FocusNode _focusNode = FocusNode();
  Color borderColor = Colors.black12;
  bool hasFocus = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    super.dispose();
    _focusNode.removeListener(_onFocusChange);
    _focusNode.dispose();
  }

  void _onFocusChange() {
    setState(() {
      hasFocus = _focusNode.hasFocus;
    });
    if (_focusNode.hasFocus) {
      setState(() {
        borderColor = Constants.mainColor;
      });
    } else {
      setState(() {
        borderColor = Colors.black12;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Space.mediumH,
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              if (hasFocus)
              BoxShadow(
                color: Constants.mainColor,
                offset: Offset(0, 0),
                blurRadius: 3,
              ),
            ],
            border: Border.all(color: borderColor, width: 2),
            borderRadius: BorderRadius.circular(10)
          ),
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  focusNode: _focusNode,
                  cursorColor: Constants.mainColor,
                  cursorWidth: 2,
                  cursorRadius: Radius.circular(5),
                  controller: widget.controller,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  decoration: InputDecoration(
                    isDense: true,
                    hintText: this.widget.label,
                    hintStyle: TextStyle(
                      color: Colors.black26,
                    ),
                    contentPadding: EdgeInsets.all(0),
                    border: InputBorder.none,
                    // hintText: hint,
                  ),
                ),
              ),
              // Container(
              //   // child: Icon(CupertinoIcons.check_mark_circled, color: Colors.green,),
              //   child: Icon(CupertinoIcons.exclamationmark_circle, color: Colors.red,),
              // )
            ],
          ),
        ),
      ],
    );
  }
}
