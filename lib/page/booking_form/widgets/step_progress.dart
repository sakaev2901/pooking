import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StepProgress extends StatelessWidget {
  final int stepIndex;
  final int stepsCount;

  const StepProgress(
      {Key? key, required this.stepIndex, required this.stepsCount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: _buildSteps(),
      ),
    );
  }

  List<Widget> _buildSteps() {
    List<Widget> widgets = [];
    for (int i = 0; i < stepsCount; i++) {
      if (i < stepIndex) {
        widgets.add(
          Icon(
            CupertinoIcons.check_mark_circled,
            color: Colors.white,
          ),
        );
      } else if (i == stepIndex) {
        widgets.add(Icon(
          CupertinoIcons.circle_filled,
          color: Colors.white,
        ));
      } else {
        widgets.add(Icon(
          CupertinoIcons.circle,
          color: Colors.white,
          size: 18,
        ));
      }
      if (i != stepsCount - 1)
      widgets.add(Container(
        color: Colors.white,
        height: 3,
        width: 40,
      ));
    }
    return widgets;
  }
}
