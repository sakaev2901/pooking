import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/booking_form/widgets/alertion_text.dart';
import 'package:pooking/page/booking_form/widgets/booking_form.dart';

class FormStep extends StatelessWidget {
  const FormStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AlertingText(),
        Space.bigH,
        BookingForm(),
      ],
    );
  }
}
