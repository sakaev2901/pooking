import 'package:flutter/material.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/user_data_info.dart';

import '../../../../../base/space.dart';
import '../../../../../config/constants.dart';
import '../../stars_grade.dart';
import '../booking_data/booking_range.dart';
import '../booking_data/total_booking_info.dart';

class PaymentBookingInfo extends StatelessWidget {
  const PaymentBookingInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(Constants.padding,),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  'Dogrerege by Hilton Los Angeels Downtown',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          Space.smallH,
          StarsGrade(),
          Space.smallH,
          Divider(height: 0,),
          BookingRange(),
          Divider(height: 0,),
          TotalBookingInfo(),
          Divider(height: 0,),
          UserDataInfo(),
          Divider(height: 0,),
        ],
      ),
    );
  }
}
