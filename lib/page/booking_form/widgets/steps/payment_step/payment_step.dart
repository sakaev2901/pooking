import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/add_coupon_button.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/free_payment_card.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/payment_booking_info.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/payment_option_list.dart';

class PaymentStep extends StatelessWidget {
  const PaymentStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PaymentOptionList(),
        Space.mediumH,
        AddCouponButton(),
        Space.mediumH,
        FreePaymentCard(),
        Space.mediumH,
        PaymentBookingInfo(),
      ],
    );
  }
}
