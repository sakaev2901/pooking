import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/add_card/add_card_page.dart';

import '../../../../../config/constants.dart';

class AddPaymentButton extends StatelessWidget {
  const AddPaymentButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => pushNewScreen(context, screen: AddCardPage()),
      child: Container(

        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(1, 1),
                    blurRadius: 5,
                  )
                ],
              ),
              child: Icon(
                CupertinoIcons.creditcard,
                color: Constants.mainColor,
              ),
            ),
            Space.mediumH,
            Text('Новая карта', style: TextStyle(fontSize: 10),),
          ],
        ),
      ),
    );
  }
}
