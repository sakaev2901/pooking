import 'package:flutter/material.dart';
import 'package:pooking/base/text_button_base.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/add_coupon_sheet.dart';

class AddCouponButton extends StatelessWidget {
  const AddCouponButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      width: double.infinity,
      child: TextButtonBase(
        text: 'Добавить промокод',
        onTap: () => showModalBottomSheet(
          context: context,
          builder: (_) => AddCouponSheet(),
          backgroundColor: Colors.transparent,
        ),
        fontSize: 14,
      ),
    );
  }
}
