import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class FreePaymentCard extends StatelessWidget {
  const FreePaymentCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(Constants.padding),
      child: Row(
        children: [
          Icon(CupertinoIcons.check_mark_circled, color: Colors.green,),
          Space.bigW,
          Text('Бесплатная отмена до 23:59 14 марта', style: TextStyle(fontWeight: FontWeight.bold,),),
        ],
      ),
    );
  }
}
