import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/add_payment_button.dart';

import '../../../../../config/constants.dart';

class PaymentOption extends StatefulWidget {
  bool selected;
  final String paymentTitle;
  final String paymentDescription;
  final int index;
  GestureTapCallback onTap;

  PaymentOption({Key? key, this.selected = false, required this.index, required this.paymentDescription, required this.paymentTitle, required this.onTap}) : super(key: key);

  @override
  State<PaymentOption> createState() => _PaymentOptionState();
}

class _PaymentOptionState extends State<PaymentOption> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.widget.onTap,
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            border: Border.all(
              color: this.widget.selected ? Constants.mainColor : Colors.grey,
              width: 2,
            )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Вариант ${this.widget.index + 1}',
                      style: TextStyle(fontSize: 10, color: Colors.grey,),
                    ),
                    Space.smallH,
                    Text(
                      this.widget.paymentTitle,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Spacer(),
                if (widget.selected)
                  Icon(
                    Icons.radio_button_checked,
                    color: Constants.mainColor,
                  )
                else
                  Icon(
                    Icons.radio_button_off,
                  )
              ],
            ),
            Space.mediumH,
            Text(this.widget.paymentDescription),
            if (this.widget.selected)
            Padding(padding: EdgeInsets.only(top: 16,),child: AddPaymentButton()),
          ],
        ),
      ),
    );
  }
}
