import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/sheet_base.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/booking_form/widgets/custom_text_input.dart';

class AddCouponSheet extends StatelessWidget {
  const AddCouponSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SheetBase(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Space.mediumH,
            Text('Добавить промокод', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
            Space.mediumH,
            CustomTextInput(label: 'Введите свой промокод'),
            Space.mediumH,
            Spacer(),
            ButtonBase(text: 'Применить', onTap: () => Navigator.of(context).pop()),
          ],
        ),
      ),
    );
  }
}
