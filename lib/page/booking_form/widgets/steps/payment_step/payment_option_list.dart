import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_form/widgets/steps/payment_step/payment_option.dart';

class PaymentOptionList extends StatefulWidget {
  int paymentType;

  PaymentOptionList({Key? key, this.paymentType = -1}) : super(key: key);

  @override
  State<PaymentOptionList> createState() => _PaymentOptionListState();
}

class _PaymentOptionListState extends State<PaymentOptionList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: EdgeInsets.all(
        Constants.padding,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Выберите варинат оплаты',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Space.mediumH,
          PaymentOption(
            paymentDescription:
                'Оплата с карты сниматься не будет. Данные вашей карты нужны только для гарантии бронирования',
            paymentTitle: 'Оплатить на месте',
            index: 0,
            selected: this.widget.paymentType == 0,
            onTap: () {
              if (widget.paymentType != 0) {
                setState(() {
                  this.widget.paymentType = 0;
                });
              }
            },
          ),
          Space.mediumH,
          PaymentOption(
            paymentDescription:
                'Ваш платеж обработает Pooking.com. Если вы измените решение мы вернем вам оплату полностью',
            paymentTitle: 'Оплатить сейчас',
            selected: this.widget.paymentType == 1,
            onTap: () {
              if (widget.paymentType != 1) {
                setState(() {
                  this.widget.paymentType = 1;
                });
              }
            },
            index: 1,
          ),
        ],
      ),
    );
  }
}
