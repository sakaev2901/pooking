import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class UserDataInfo extends StatelessWidget {
  const UserDataInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: Constants.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Сакаев Эльдар', style: TextStyle(fontWeight: FontWeight.bold),),
          Space.smallH,
          Text('sakaev2901@gmail.com', style: TextStyle(),),
          Space.smallH,
          Text('89613675766', style: TextStyle(),),
        ],
      ),
    );
  }
}
