import 'package:flutter/material.dart';

import '../../../../../base/space.dart';

class TotalPrice extends StatelessWidget {
  const TotalPrice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.mediumH,
              Text('Итого', style: TextStyle(fontWeight: FontWeight.bold,),),
              Space.smallH,
              Text('1 номер, 5 ночей', style: TextStyle(fontSize: 12, ),),
              Space.mediumH,
            ],
          ),
          Spacer(),
          Text('23 324 руб', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
        ],
      ),
    );
  }
}
