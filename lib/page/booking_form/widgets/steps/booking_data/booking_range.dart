import 'package:flutter/material.dart';

class BookingRange extends StatelessWidget {
  const BookingRange({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                children: [
                  Text('Регистрация заезда'),
                  Text('вт - 15 мар. - 2022', style: TextStyle(fontWeight: FontWeight.bold,),),
                ],
              ),
            ),
            VerticalDivider(width: 0,),
            Expanded(
              child: Column(
                children: [
                  Text('Регистрация отъезда'),
                  Text('вт - 18 мар. - 2022', style: TextStyle(fontWeight: FontWeight.bold,),),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
