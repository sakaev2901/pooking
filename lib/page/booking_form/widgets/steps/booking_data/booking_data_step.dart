import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_form/widgets/stars_grade.dart';
import 'package:pooking/page/booking_form/widgets/steps/booking_data/hotel_info.dart';
import 'package:pooking/page/hotel/widgets/hotel_title.dart';

class BookingDataStep extends StatelessWidget {
  const BookingDataStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [

        HotelInfo(),

      ],
    );
  }
}
