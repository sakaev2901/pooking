import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';

class TotalBookingInfo extends StatelessWidget {
  const TotalBookingInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Space.mediumH,
          Text('Вы выбрали'),
          Space.mediumH,
          Text('5 ночей, 1 номер для 3 взрослых', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,),),
          Space.mediumH,

        ],
      ),
    );
  }
}
