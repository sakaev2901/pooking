import 'package:flutter/material.dart';
import 'package:pooking/page/booking_form/widgets/steps/booking_data/booking_range.dart';
import 'package:pooking/page/booking_form/widgets/steps/booking_data/total_booking_info.dart';
import 'package:pooking/page/booking_form/widgets/steps/booking_data/total_price.dart';

import '../../../../../base/space.dart';
import '../../../../../config/constants.dart';
import '../../../../hotel/widgets/hotel_title.dart';
import '../../stars_grade.dart';

class HotelInfo extends StatelessWidget {
  final Color color;

  const HotelInfo({Key? key, this.color = Colors.white,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      color: color,
      padding: EdgeInsets.all(Constants.padding,),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(height: MediaQuery.of(context).size.height / 4,width: double.infinity, child: Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRz-T1VPQomUShQwNLVOfw5j_3wM8GlBbRd8w&usqp=CAU', fit: BoxFit.cover,)),
          ),
          Space.bigH,
          Row(
            children: [
              Expanded(
                child: Text(
                  'Dogrerege by Hilton Los Angeels Downtown',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              ReviewGrade(grade: 8.8),
            ],
          ),
          Space.smallH,
          StarsGrade(),
          Space.mediumH,
          Text('Россия, г. Казань, ул. Профсоюзная, д. 45'),
          Space.bigH,
          Divider(height: 0,),
          BookingRange(),
          Divider(height: 0,),
          TotalBookingInfo(),
          Divider(height: 0,),
          TotalPrice(),
          Divider(height: 0,),
        ],
      ),
    );
  }
}
