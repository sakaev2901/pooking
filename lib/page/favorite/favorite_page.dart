import 'package:flutter/material.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';

import '../hotel_list/filter_list.dart';
import '../hotel_list/hotel_item.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      body: Column(
        children: [
          SizedBox(height: 10,),
          HotelItem(liked: true,),
          SizedBox(height: 10,),
          HotelItem(liked: true,),
          SizedBox(height: 10,),
          HotelItem(liked: true,),
          SizedBox(height: 10,),

        ],
      ),
      appBar: AppTitleBase(text: 'Сохраненное',),
    );
  }
}
