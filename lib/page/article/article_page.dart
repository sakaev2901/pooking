import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class ArticlePage extends StatelessWidget {
  final String url;
  final String title;

  const ArticlePage({Key? key, required this.url, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
              constraints: BoxConstraints(
                minHeight: double.infinity,
                minWidth: double.infinity,
              ),
              child: Hero(
                tag: url,
                child: Stack(
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        minHeight: double.infinity,
                        minWidth: double.infinity,
                      ),
                      child: Image.network(
                        url,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      color: Color(0x1B000000),
                    ),
                  ],
                ),
              )),

          // Container(
          //   color: Colors.red,
          // ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Expanded(child: Container()),
                      Container(
                        width: double.infinity,
                        color: Colors.black12,
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: 15, right: 15, top: 15 + MediaQuery.of(context).padding.top, bottom: 15),
                                child: Icon(
                                  CupertinoIcons.back,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: Constants.padding),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              style: TextStyle(
                                fontSize: 30,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Space.bigH,
                            Text(
                              'Travel.ru — информационный ресурс и онлайн-путеводитель по более чем 230 странам мира[12]. Ежемесячная аудитория Travel.ru — 1-2 миллиона уникальных посетителей в зависимости от сезона. Сайт входит в 30 самых посещаемых туристических порталов Рунета[13]. Наряду с материалами, подготовленными непосредственно сотрудниками компании, на сайте также публикуются материалы из блогов, форумов и других открытых источников со ссылками на авторов. Актуальность и достоверность публикуемой информации проверяется редакторами портала. В структуру сайта входят визовый раздел, в котором описаны правила получения визы в каждую страну в зависимости от места подачи документов, интерактивная обновляемая карта безвизовых стран, разделы справочной информации по таможенному законодательству разных стран, правилам перелетов различных авиакомпаний и путеводитель по мировым достопримечательностям[1].',
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                              ),
                            ),
                            Space.extraH,
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
