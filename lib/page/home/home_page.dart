import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/home/widgets/banner/small_banner.dart';
import 'package:pooking/page/home/widgets/filter_card.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  AnnotatedRegion<SystemUiOverlayStyle>(
      value: FlexColorScheme.themedSystemNavigationBar(
        context,
        opacity: 0,
        useDivider: false,
      ),
      child: BaseScreen(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15,
            ),
            FilterCard(),
            SizedBox(
              height: 15,
            ),
            Text('Лента', style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700, color: Colors.grey),),
            Space.bigH,
            Row(
              children: [
                Expanded(
                  child: SmallBanner(
                    url:
                        'https://img.championat.com/news/big/v/r/na-kraju-zemli-10-neobychnyh-idej-dlja-puteshestvija_1517331980822771222.jpg',
                    title: 'Куда слетать?',
                    text: 'Посмотреть интересные места',
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: SmallBanner(
                  url:
                      'https://cdn.shrm.org/image/upload/c_crop,h_704,w_1253,x_0,y_3/c_fit,f_auto,q_auto,w_767/v1/News/COVID_biz_travel_df84vb?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjMsIngyIjoxMjUzLCJ5MiI6NzA3LCJ3IjoxMjUzLCJoIjo3MDR9fQ%3D%3D',
                      title: 'Ограничения',
                      text: 'Посмотреть ограничения ковид',
                ),),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            SmallBanner(
              horizontalExpanded: true,
              url:
                  'https://www.workaway.info/gfx/2015/content/frontpage/header_frontpage_5.jpg',
              title: 'Топ 3 городов',
              text: 'Посмотреть интересные места',
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Expanded(
                    child: SmallBanner(
                  url: 'https://res.klook.com/image/upload/Mobile/City/szhx3ytpgfnhpbmsngk0.jpg',
                      title: 'Нью Йорк',
                      text: 'Гид по прекрасному городу Нью Йорк',
                )),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: SmallBanner(
                  url:
                      'https://thumbs.dreamstime.com/b/%D1%82%D1%80%D0%BE%D0%BF%D0%B8%D0%BA-%D0%B7%D0%B0%D1%85%D0%BE%D0%B4%D0%B0-%D1%81%D0%BE%D0%BB%D0%BD%D1%86%D0%B0-%D0%BF%D0%B5%D1%81%D0%BA%D0%B0-%D0%BB%D0%B0%D0%B4%D0%BE%D0%BD%D0%B5%D0%B9-%D0%BA%D0%BE%D0%BA%D0%BE%D1%81%D0%B0-%D0%BF%D0%BB%D1%8F%D0%B6%D0%B0-13157310.jpg',
                      title: 'Любите тепло?',
                      text: 'Посмотреть интересные места',
                )),
              ],
            ),
            SizedBox(
              height: 15,
            ),
          ],
        ),
        appBar: const AppTitleBase(
          text: 'BaR',
          isMain: true,
        ),
      ),
    );
  }
}
