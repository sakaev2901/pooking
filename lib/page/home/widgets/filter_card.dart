import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/page/home/widgets/input_preview.dart';
import 'package:pooking/page/home/widgets/range_calendar.dart';
import 'package:pooking/page/hotel_list/hotel_list_page.dart';
import 'package:pooking/page/search/search_page.dart';

import '../../../feature/filter_sheet/filter_sheet.dart';

class FilterCard extends StatelessWidget {
  const FilterCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: Colors.black12, width: 4),
      ),
      child: Column(
        children: [
          InputPreview(
            text: 'Рядом со мной',
            iconData: CupertinoIcons.search,
            onTap: () => pushNewScreen(context, screen: SearchPage(), withNavBar: false)
          ),
          Divider(height: 0, color: Colors.black12, thickness: 4,),
          InputPreview(
            text: '14 мар, пн - 20 мар, вс',
            iconData: CupertinoIcons.calendar,
            onTap: () => showModalBottomSheet(context: context, builder: (ctx) => RangeCalendar(), backgroundColor: Colors.transparent),
          ),
          Divider(height: 0, color: Colors.black12, thickness: 4,),
          InputPreview(
            text: '2 номера - 3 взрослых - 0 детей',
            iconData: CupertinoIcons.profile_circled,
            onTap: () => showModalBottomSheet(context: context, builder: (ctx) => FilterSheet(), backgroundColor: Colors.transparent),
          ),
          Divider(height: 0,),
          ButtonBase(text: 'Найти', onTap: () => pushNewScreen(context, screen: HotelListPage(), withNavBar: false)),
        ],
      ),
    );
  }
}
