import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class InputPreview extends StatelessWidget {
  final String text;
  final IconData iconData;
  final GestureTapCallback onTap;

  const InputPreview({Key? key, required this.text,required this.iconData, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: BoxDecoration(
          // border: Border.all(color: Colors.grey, width: 2),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            Icon(
              iconData,
              color: Colors.grey,
            ),
            SizedBox(
              width: 15,
            ),
            Text(
              text,
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15, color: Colors.black54),
            ),
          ],
        ),
      ),
    );
  }
}
