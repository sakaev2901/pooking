import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarRangeInput extends StatefulWidget {
  const CalendarRangeInput({Key? key}) : super(key: key);

  @override
  _CalendarRangeInputState createState() => _CalendarRangeInputState();
}

class _CalendarRangeInputState extends State<CalendarRangeInput> {

  DateTime _focusedDay = DateTime.now();

  DateTime? _selectedDay;

  DateTime? _rangeStart;

  RangeSelectionMode _rangeSelectionMode = RangeSelectionMode
      .toggledOn; // Can be toggled on/off by longpressing a date

  DateTime? _rangeEnd;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              WeekDayText(day: 'ПН'),
              WeekDayText(day: 'ВТ'),
              WeekDayText(day: 'СР'),
              WeekDayText(day: 'ЧТ'),
              WeekDayText(day: 'ПТ'),
              WeekDayText(day: 'СБ'),
              WeekDayText(day: 'ВС'),
            ],
          ),
        ),
        TableCalendar(
          headerVisible: false,
          daysOfWeekVisible: false,
          focusedDay: _focusedDay,
          startingDayOfWeek: StartingDayOfWeek.monday,
          firstDay: DateTime.utc(2010, 10, 16),
          lastDay: DateTime.utc(2030, 3, 14),
          selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
          rangeStartDay: _rangeStart,
          rangeEndDay: _rangeEnd,
          rangeSelectionMode: _rangeSelectionMode,
          onDaySelected: (selectedDay, focusedDay) {
            if (!isSameDay(_selectedDay, selectedDay)) {
              setState(() {
                _selectedDay = selectedDay;
                _focusedDay = focusedDay;
                _rangeStart = null; // Important to clean those
                _rangeEnd = null;
                _rangeSelectionMode = RangeSelectionMode.toggledOff;
              });
            }
          },
          onRangeSelected: (start, end, focusedDay) {
            setState(() {
              _selectedDay = null;
              _focusedDay = focusedDay;
              _rangeStart = start;
              _rangeEnd = end;
              _rangeSelectionMode = RangeSelectionMode.toggledOn;
            });
          },
          calendarStyle: CalendarStyle(
            todayDecoration: BoxDecoration(),
            todayTextStyle: TextStyle(
              fontSize: 16,
              color: Colors.black,
            ),
            defaultTextStyle: TextStyle(
              fontSize: 16,
              color: Colors.black,
            ),
          ),
        ),
      ],
    );
  }
}

class WeekDayText extends StatelessWidget {
  final String day;

  const WeekDayText({Key? key, required this.day}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      this.day,
      style: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.bold,
        color: Color(0xFF979CA9),
      ),
    );
  }
}

