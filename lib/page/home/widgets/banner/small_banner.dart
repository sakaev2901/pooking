import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/config/color_scheme_extension.dart';
import 'package:pooking/page/article/article_page.dart';

class SmallBanner extends StatelessWidget {
  final double ratio;
  final bool horizontalExpanded;
  final String url;
  final String title;
  final String text;

  const SmallBanner({Key? key, this.ratio = 0.85, this.horizontalExpanded = false, required this.url, required this.title, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildBanner(context);
  }

  Widget _buildBanner(BuildContext context) {
    double borderRadius = 12;
    final TextStyle style = Theme.of(context).textTheme.heroText;

    Widget container = InkWell(
      onTap: () => pushNewScreen(context, screen: ArticlePage(url: url, title: title,), withNavBar: false, pageTransitionAnimation: PageTransitionAnimation.fade),
      child: Container(
        height: MediaQuery.of(context).size.width / 2,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(4,4),
                blurRadius: 4
            )
          ],
          borderRadius: BorderRadius.circular(borderRadius),
        ),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(borderRadius),
              child: Container(
                color: Colors.black12,
                constraints: BoxConstraints(
                  minHeight: double.infinity,
                  minWidth: double.infinity,
                ),
                child: Hero(
                  tag: url,
                  child: Image.network(
                    url,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20,),
                      ),
                      SizedBox(height: 30,),
                      Text(
                        text,
                        style: TextStyle(fontWeight: FontWeight.w500, color: Colors.white, fontSize: 13,),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    if (horizontalExpanded) {
      return Container( width:  double.infinity,   height: MediaQuery.of(context).size.width / 2.5,child: container);
    } else {
      return AspectRatio(
          aspectRatio: this.ratio,
          child: container,
      );
    }
  }
}
