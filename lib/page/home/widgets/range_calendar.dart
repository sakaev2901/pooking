import 'package:flutter/material.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/sheet_base.dart';
import 'package:pooking/page/home/widgets/calendar_range_input.dart';
import 'package:table_calendar/table_calendar.dart';

class RangeCalendar extends StatefulWidget {

  const RangeCalendar({Key? key}) : super(key: key);

  @override
  State<RangeCalendar> createState() => _RangeCalendarState();
}

class _RangeCalendarState extends State<RangeCalendar> {


  @override
  Widget build(BuildContext context) {
    return SheetBase(
      child: Container(
        child: Column(
          children: [
            SizedBox(height: 20,),
            CalendarRangeInput(),
            SizedBox(height: 20,),
            ButtonBase(text: 'Выбрать даты', onTap: () => Navigator.of(context).pop()),
          ],
        ),
      ),
    );
  }
}
