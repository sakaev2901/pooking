import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_button.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/booking_form/widgets/custom_text_input.dart';

class UserDataPage extends StatelessWidget {
  const UserDataPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      body: Column(
        children: [
          Space.bigH,
          CustomTextInput(label: 'Имя'),
          Space.mediumH,
          CustomTextInput(label: 'Фамилия'),
          Space.mediumH,
          CustomTextInput(label: 'Почта'),
          Space.mediumH,
          CustomTextInput(label: 'Номер телефона'),
          Space.mediumH,
          ButtonBase(text: 'Сохранить', onTap: () => {}),
          Space.bigH,
          CustomTextInput(label: 'Старый пароль'),
          Space.mediumH,
          CustomTextInput(label: 'Новый пароль'),
          Space.mediumH,
          CustomTextInput(label: 'Повторите пароль'),
          Space.mediumH,
          ButtonBase(text: 'Сохранить', onTap: () => {}),
          Space.extraH,
        ],
      ),
      appBar: AppTitleBase(text: 'Изменить данные', withBackButton: true,),
    );
  }
}
