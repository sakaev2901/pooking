import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

import '../../hotel/widgets/hotel_title.dart';

class GradeBlock extends StatelessWidget {
  final double grade;
  final String gradeText;
  final int reviewCount;

  const GradeBlock({
    Key? key,
    required this.grade,
    required this.gradeText,
    required this.reviewCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Constants.padding),
      color: Colors.white,
      child: Row(
        children: [
          ReviewGrade(grade: grade),
          SizedBox(
            width: 8,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                gradeText,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text('$reviewCount отзывов'),
            ],
          ),
        ],
      ),
    );
  }
}
