import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/hotel/widgets/hotel_title.dart';

class UserReviewCard extends StatelessWidget {
  const UserReviewCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(Constants.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(child: Icon(CupertinoIcons.profile_circled),),
              SizedBox(width: 8,),
              Column(children: [
                Text('Эльдар', style: TextStyle(fontWeight: FontWeight.bold,),),
                Text('Россия'),
              ],),
              Spacer(),
              ReviewGrade(grade: 9),
            ],
          ),
          SizedBox(height: 8,),
          Text('28 мар. 2020'),
          Text('Все заебись', style: TextStyle(fontSize: 20, fontWeight:  FontWeight.bold,),),
          SizedBox(height: 18,),
          Text(Constants.text, ),
        ],
      ),
    );
  }
}
