import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/review_list/widgets/grade_block.dart';
import 'package:pooking/page/review_list/widgets/user_review_card.dart';

import '../../base/app_title.dart';
import '../../base/base_screen.dart';
import '../hotel/widgets/hotel_title.dart';

class ReviewListPage extends StatelessWidget {
  const ReviewListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      backgroundColor: Constants.backgroundColor,
      horizontalPadding: 0,
      body: Column(
        children: [
          SizedBox(height: 8,),
          GradeBlock(grade: 8.8, gradeText: 'Хорошо', reviewCount: 33),
          SizedBox(height: 4,),
          UserReviewCard(),
          SizedBox(height: 4,),
          UserReviewCard(),
          SizedBox(height: 4,),
          UserReviewCard(),
        ],
      ),
      appBar: AppTitleBase(text: 'Отзывы', withBackButton: true,),
    );
  }
}
