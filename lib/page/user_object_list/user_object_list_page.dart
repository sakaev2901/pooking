import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';

class UserObjectListPage extends StatelessWidget {
  const UserObjectListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      body: Column(
        children: [

        ],
      ),
      appBar: AppTitleBase(text: 'Мои объекты', withBackButton: true,),
    );
  }
}
