import 'package:flutter/material.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';

class FilterPage extends StatelessWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      body: Column(
        children: [],
      ),
      appBar: AppTitleBase(
        text: 'Фильтры',
        withBackButton: true,
      ),
    );
  }
}
