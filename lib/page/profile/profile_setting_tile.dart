import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class ProfileSettingTile extends StatelessWidget {
  final String text;
  final IconData iconData;
  final Widget screen;
  final Color iconColor;
  final Color textColor;

  const ProfileSettingTile(
      {Key? key,
      required this.iconData,
      required this.text,
        this.iconColor = Constants.mainColor,
        this.textColor = Colors.grey,
      required this.screen})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => pushNewScreen(context, screen: screen, withNavBar: false),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 15,),
        child: Row(
          children: [
            Icon(iconData, color: iconColor, size: 20,),
            Space.extraW,
            Text(text, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: textColor,),),
          ],
        ),
      ),
    );
  }
}
