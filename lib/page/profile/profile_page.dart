import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/post_object/post_object_page.dart';
import 'package:pooking/page/profile/profile_setting_tile.dart';
import 'package:pooking/page/profile/user_info_card.dart';
import 'package:pooking/page/user_data/user_data_page.dart';
import 'package:pooking/page/user_object_list/user_object_list_page.dart';

import '../login/login_page.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      horizontalPadding: 0,
      body: Column(
        children: [
          UserInfoCard(),
          Space.bigH,
          Divider(height: 0, thickness: 8, color: Colors.black12,),
          ProfileSettingTile(iconData: CupertinoIcons.settings, text: 'Упраление аккаунтом', screen: UserDataPage()),
          ProfileSettingTile(iconData: CupertinoIcons.bed_double_fill, text: 'Разместить объект', screen: PostObjectPage()),
          ProfileSettingTile(iconData: CupertinoIcons.building_2_fill, text: 'Мои объекты', screen: UserObjectListPage()),
          Divider(height: 0, thickness: 1,),
          ProfileSettingTile(iconData: Icons.exit_to_app, text: 'Выйти', screen: LoginPage(), textColor: Colors.red, iconColor: Colors.red,),
        ],
      ),
      appBar: AppTitleBase(text: 'Профиль',),
    );
  }
}
