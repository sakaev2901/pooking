import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/config/constants.dart';

class UserInfoCard extends StatelessWidget {
  const UserInfoCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Constants.padding,),
      child: Column(
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage('https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
                radius: 50,
              ),
              Space.bigW,
              Text('Сакаев Эльдар', style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700, color: Constants.mainColor),),
            ],
          ),
          Space.bigH,
          UserInfoTile(iconData: CupertinoIcons.phone, text: '7-961-367-57-66'),
          Space.mediumH,
          UserInfoTile(iconData: CupertinoIcons.mail, text: 'sakaek2901@gmail.com'),
        ],
      ),
    );
  }
}

class UserInfoTile extends StatelessWidget {
  final IconData iconData;
  final String text;

  const UserInfoTile({Key? key, required this.iconData, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Icon(iconData, color: Colors.black38, size: 20,),
          Space.extraW,
          Text(text, style: TextStyle(color: Colors.black38, fontWeight: FontWeight.bold,),),
        ]
      ),
    );
  }
}

