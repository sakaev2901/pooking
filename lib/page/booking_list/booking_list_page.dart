import 'package:flutter/material.dart';
import 'package:pooking/base/app_title.dart';
import 'package:pooking/base/base_screen.dart';
import 'package:pooking/page/home/widgets/filter_card.dart';

import '../../base/space.dart';
import 'widgets/booking_tile.dart';

class BookingListPage extends StatelessWidget {
  const BookingListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      body: Column(
        children: [
          Space.extraH,
          BookingTile(),
          Space.mediumH,
          BookingTile(),
          Space.mediumH,
          BookingTile(),
          Space.mediumH,
          BookingTile(),
        ],
      ),
      appBar:  const AppTitleBase(text: 'Ваши поездки',),
    );
  }
}
