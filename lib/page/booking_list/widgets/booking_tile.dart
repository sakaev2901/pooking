import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/base/card_base.dart';
import 'package:pooking/base/space.dart';
import 'package:pooking/page/booking_data/booking_data_page.dart';
import 'package:pooking/page/booking_list/widgets/booking_status.dart';

class BookingTile extends StatelessWidget {
  const BookingTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardBase(
      padding: EdgeInsets.all(12),
      onTap: () => pushNewScreen(
        context,
        screen: BookingDataPage(),
        withNavBar: false,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text('Hotel name', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15,),),
              Spacer(),
              BookingStatus(text: 'активный'),
            ],
          ),
          Space.mediumH,
          Text('2 номера для 4 взрослых', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.grey,),),
          Space.mediumH,
          Text('14-01-2022 - 16-01-2022', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.grey,),),
        ],
      ),
    );
  }
}
