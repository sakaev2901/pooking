import 'package:flutter/material.dart';

class BookingStatus extends StatelessWidget {
  final Color color;
  final String text;

  const BookingStatus({Key? key, this.color = Colors.green, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(6)
      ),
      child: Text(
        text,
        style: TextStyle(
          color: Colors.white,
          fontSize: 10,
        ),
      ),
    );
  }
}
