import 'package:flutter/material.dart';
import 'package:pooking/base/sheet_base.dart';
import 'package:pooking/base/space.dart';

import '../../base/base_button.dart';
import '../../config/constants.dart';

class FilterSheet extends StatelessWidget {
  const FilterSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SheetBase(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Укажите количество номеров и гостей',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          FilterSheetInput(text: 'Номера'),
          SizedBox(
            height: 10,
          ),
          FilterSheetInput(text: 'Взрослых'),
          SizedBox(
            height: 10,
          ),
          FilterSheetInput(text: 'Детей'),
          Spacer(),
          ButtonBase(
            text: 'Применить',
            onTap: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );
  }
}

class FilterSheetInput extends StatelessWidget {
  final String text;

  const FilterSheetInput({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text(
            text,
            style: TextStyle(fontSize: 15),
          ),
          Spacer(),
          FilterSheetInputButton(iconData: Icons.remove,),
          Container(
            width: 40,
            child: Center(
              child: Text(
                '1',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          FilterSheetInputButton(iconData: Icons.add,),
        ],
      ),
    );
  }
}

class FilterSheetInputButton extends StatelessWidget {
  final IconData iconData;

  const FilterSheetInputButton({Key? key, required this.iconData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {},
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3),
            border: Border.all(
              color: Constants.mainColor,
            )),
        child: Icon(iconData),
      ),
    );
  }
}
