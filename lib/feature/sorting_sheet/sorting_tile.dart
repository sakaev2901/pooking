import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../config/constants.dart';

class SortingTile extends StatelessWidget {
  final String text;

  const SortingTile({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => {},
      child: Container(
        padding: EdgeInsets.only(left: 0, right: 15, top: 10, bottom: 10),
        child: Row(
          children: [
            Text(text),
            Spacer(),
            Icon(CupertinoIcons.largecircle_fill_circle, color: Constants.mainColor,),
          ],
        ),
      ),
    );
  }
}
