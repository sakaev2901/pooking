import 'package:flutter/material.dart';
import 'package:pooking/base/sheet_base.dart';
import 'package:pooking/feature/sorting_sheet/sorting_tile.dart';

class SortingSheet extends StatelessWidget {
  const SortingSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SheetBase(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20,),
            const Text(
              'Сортировать',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SortingTile(text: 'Расстояние до города'),
            SortingTile(text: 'Популярность'),
            SortingTile(text: 'Цена по возростанию'),
            SortingTile(text: 'Цена по убыванию'),
          ],
        ),
      ),
    );
  }
}
