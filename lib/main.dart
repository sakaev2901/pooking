import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pooking/page/home/home_page.dart';
import 'package:pooking/tabs.dart';

import 'config/constants.dart';

void main() {
  runZonedGuarded(() {
    runApp(DevicePreview(
      enabled: true,
      builder: (context) => MyApp(),

    ),);
  }, (dynamic error, dynamic stack) {
    print(error);
    print(stack);
  });



}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      useInheritedMediaQuery: true,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      home: AnnotatedRegion<SystemUiOverlayStyle>(
          value: FlexColorScheme.themedSystemNavigationBar(
            context,
            opacity: 0,
            useDivider: false,
          ),child: MainTabs()),
    );
  }
}



