import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pooking/config/constants.dart';
import 'package:pooking/page/booking_list/booking_list_page.dart';
import 'package:pooking/page/favorite/favorite_page.dart';
import 'package:pooking/page/home/home_page.dart';
import 'package:pooking/page/profile/profile_page.dart';

class MainTabs extends StatelessWidget {
  PersistentTabController _controller = PersistentTabController(initialIndex: 0);


  MainTabs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      // bottomScreenMargin: 50,
      controller: _controller,
      screens: [HomePage(), FavoritePage(), BookingListPage(), ProfilePage()],
      items: _navBarsItems(),
      confineInSafeArea: true,
      // margin: EdgeInsets.symmetric(vertical: 15),
      backgroundColor: Color(0xFFFAFAFA), // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset: true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows: true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        gradient: null,
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,

      itemAnimationProperties: ItemAnimationProperties( // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation( // Screen transition animation on change of selected tab.
        animateTabTransition: false,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle: NavBarStyle.style6, // Choose the nav bar style with this property.
    );
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    TextStyle style = TextStyle(
      fontSize: 10,
      fontWeight: FontWeight.bold,
    );
    return [
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.search),
        title: ("Найти"),
        activeColorPrimary: Constants.mainColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
        textStyle: style,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.heart),
        title: ("Сохраненное"),
        activeColorPrimary: Constants.mainColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
        textStyle: style,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.tickets),
        title: ("Бронирования"),

        activeColorPrimary: Constants.mainColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
        textStyle: style,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.profile_circled),
        title: ("Профиль"),
        activeColorPrimary: Constants.mainColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
        textStyle: style,
      ),
    ];
  }


}
