import 'package:flutter/material.dart';

abstract class Space {

  static final SizedBox smallH = SizedBox(height: 4,);
  static final SizedBox smallW = SizedBox(width: 4,);
  static final SizedBox mediumH = SizedBox(height: 8,);
  static final SizedBox mediumW = SizedBox(width: 8,);
  static final SizedBox bigH = SizedBox(height: 16,);
  static final SizedBox bigW = SizedBox(width: 16,);
  static final SizedBox extraH = SizedBox(height: 24,);
  static final SizedBox extraW = SizedBox(width: 24,);
}
