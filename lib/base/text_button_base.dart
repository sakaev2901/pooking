import 'package:flutter/material.dart';

import '../config/constants.dart';

class TextButtonBase extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;
  final double fontSize;

  const TextButtonBase({
    Key? key,
    required this.text,
    required this.onTap,
    this.fontSize = 24,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 10),

            child: Align(
              alignment: Alignment.center,
              child: Text(
                text,
                style: TextStyle(
                  color: Constants.mainColor,
                  fontWeight: FontWeight.bold,
                  fontSize: fontSize,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
