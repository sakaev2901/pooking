import 'package:flutter/material.dart';

import '../config/constants.dart';

class OutlinedButtonBase extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;
  final double fontSize;

  const OutlinedButtonBase({
    Key? key,
    required this.text,
    this.fontSize = 24,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Constants.mainColor,)
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 10),

            child: Align(
              alignment: Alignment.center,
              child: Text(
                text,
                style: TextStyle(
                  color: Constants.mainColor,
                  fontSize: this.fontSize,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
