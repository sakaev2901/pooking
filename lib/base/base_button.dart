import 'package:flutter/material.dart';

import '../config/constants.dart';

class ButtonBase extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;
  final double fontSize;
  final Color backgroundColor;

  const ButtonBase({
    Key? key,
    required this.text,
    required this.onTap,
    this.fontSize = 24,
    this.backgroundColor = Constants.mainColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(10)
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 10),

            child: Align(
              alignment: Alignment.center,
              child: Text(
                text,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: fontSize,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
