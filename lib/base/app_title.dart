import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pooking/config/constants.dart';

class AppTitleBase extends StatelessWidget {
  final bool isMain;
  final String text;
  final bool withBackButton;
  const AppTitleBase({Key? key, this.isMain = false, required this.text, this.withBackButton = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 90,
      decoration: BoxDecoration(
        // color: Constants.mainColor,
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(0, 0),
            blurRadius: 8
          )
        ]
      ),
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top, left: 15, right: 15),
      child: Align(
        alignment: isMain ? Alignment.center : Alignment.centerLeft,
        child: Row(
          children: [
            if (withBackButton)
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () => Navigator.of(context).pop(),
              child: Padding(
                padding: const EdgeInsets.only(right: 15),
                child: const Icon(CupertinoIcons.back, color: Constants.mainColor,),
              ),
            ),
            Text(
              text,
              style: TextStyle(
                color: Constants.mainColor,
                fontSize: isMain ? 26 : 22,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
