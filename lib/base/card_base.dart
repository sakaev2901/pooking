import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class CardBase extends StatelessWidget {
  final Widget child;
  GestureTapCallback? onTap;
  final EdgeInsets padding;

  CardBase({Key? key, required this.child, this.onTap, this.padding = EdgeInsets.zero}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: padding,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                offset: Offset(3, 3),
                blurRadius: 9,
              ),
              BoxShadow(
                color: Colors.white10,
                offset: Offset(-1, -1),
                blurRadius: 3,
              )
            ]),
        child: child,
      ),
    );
  }
}
