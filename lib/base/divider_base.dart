import 'package:flutter/material.dart';

class DividerBase extends StatelessWidget {
  const DividerBase({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(height: 0, thickness: 8, color: Colors.black12,);
  }
}
