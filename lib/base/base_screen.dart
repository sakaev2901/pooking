import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pooking/config/constants.dart';

class BaseScreen extends StatefulWidget {
  final Widget body;
  final Widget? appBar;
  final bool showSafeAreaTopPadding;
  Color? backgroundColor;
  final double horizontalPadding;
  final Widget? bottomButton;

  BaseScreen(
      {Key? key,
      required this.body,
      this.appBar,
      this.bottomButton,
      this.horizontalPadding = Constants.padding,
      this.backgroundColor,

      this.showSafeAreaTopPadding = false})
      : super(key: key);

  @override
  State<BaseScreen> createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: FlexColorScheme.themedSystemNavigationBar(
        context,
        opacity: 0,
        useDivider: false,
      ),
      child: Scaffold(
        backgroundColor: widget.backgroundColor ?? Constants.highlightColor,
        body: SafeArea(
          top: this.widget.showSafeAreaTopPadding,
          bottom: false,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      widget.appBar ?? Container(),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: widget.horizontalPadding,
                        ),
                        child: widget.body,
                      ),
                    ],
                  ),
                ),
              ),
              if (widget.bottomButton != null) Container(
                color: Colors.white,
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
                child: widget.bottomButton!,
              ),

            ],
          ),
        ),
      ),
    );
  }
}
