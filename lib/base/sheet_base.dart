import 'package:flutter/material.dart';
import 'package:pooking/base/space.dart';

class SheetBase extends StatelessWidget {
  final Widget child;

  const SheetBase({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.transparent,
        child: Container(
          padding: EdgeInsets.only(
            left: 15,
            right: 15,
            top: 10,
            bottom: 15,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 6,
                    width: 50,
                    decoration: BoxDecoration(
                      color: Colors.black26,
                      borderRadius: BorderRadius.circular(6),
                    ),
                  )
                ],
              ),
              Space.mediumH,
              Expanded(child: child),
            ],
          ),
        ));
  }
}
